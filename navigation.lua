require ("lib.classy")

function dist(x1,y1, x2,y2)
	return ((x2-x1)^2+(y2-y1)^2)^0.5
end
function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end
function lerp2(a,b,t) return a+(b-a)*t end

lg = love.graphics

class "Navigation"
{
	enabled = false;
	clicked = {x=0, y=0, d=false};
	
	sustemindexold = 0;
	systemindex = 0;
	systems = {};
	names = {
		"Alpha";
		"Centori";
		"Beta";
		"Epsilon";
		"Gamma";
	};
	
	-- radar lines
	lines = {};
	lineindex = 1;
	timer = 0;
	speed = 0.01;
	
	offset = -285;
	mtimer = 0;
	navto = {};
	
	-- radar line color
	scancolor = {0, 255, 0};
	
	-- data bar
	dockbar;
	
	-- radar outline
	bezel = nil;
	
	warning = false;
	woffset = -85; --300

	Navigation = function(self)
		self:Generate(nil)
		self.navto = {
			naving=false;
			planetary=true;
			pindex=0;
			x = 0;
			y = 0;
		}
		
		self.dockbar = new "Dockbar"

		for i=1,275 do
			table.insert(self.lines, 0)
		end
		
		self.bezel = lg.newImage("data/bezel.png")
	end;

	Enable = function(self)
		self.enabled = true
	end;
	Disable = function(self)
		self.enabled = false
	end;
	Toggle = function(self)
		self.enabled = not self.enabled
	end;

	Update = function(self, dt)
		self.mtimer = self.mtimer + dt
		
		if self.enabled then
			self.offset = cerp(10, self.offset, 0.8)
		else
			self.offset = cerp(-285, self.offset, 0.8)
		end
		
		if self.warning then
			self.woffset = cerp(300, self.woffset, 0.8)
		else
			self.woffset = cerp(-85, self.woffset, 0.8)
		end
		
		self.dockbar:Update(dt)

		if self.lineindex == 1 then
			self.speed = 1.
		else
			self.speed = 0.01
		end
		self.timer = self.timer + dt
		if self.timer > self.speed then
			self.timer = 0
			self.lines[self.lineindex] = 155
			self.lineindex = self.lineindex + 1
			if self.lineindex > #self.lines then
				self.lineindex = 1
			end
		end
		
		-- spin galaxy planets
		if self.systemindex > 0 then
			for i,planet in ipairs(self.systems[self.systemindex].galaxy) do
				if planet.type ~= "sun" then
					local cx = 275/2
					local cy = 275/2
					local rot = (dt)/planet.speed * 16
					
					local dx = planet.x
					local dy = planet.y
					
					newx = math.cos(rot) * (dx-cx) - math.sin(rot) * (dy-cy) + cx
					newy = math.sin(rot) * (dx-cx) + math.cos(rot) * (dy-cy) + cy
					
					planet.x = newx
					planet.y = newy
					
					if planet.moon ~= nil then
						local mdx = 1
						local mdy = 1
						
						local mcx = dx
						local mcy = dy
						
						nx = math.cos(dt) * (mdx-mcx) - math.sin(dt) * (mdy-mcy) + mcx
						ny = math.sin(dt) * (mdx-mcx) + math.cos(dt) * (mdy-mcy) + mcy
						
						planet.moon.x = planet.x + math.cos(planet.moon.i * self.mtimer)*planet.moon.dx
						planet.moon.y = planet.y + math.sin(planet.moon.i * self.mtimer)*planet.moon.dy
					end
					
				end
			end
		end
	end;

	Draw = function(self)
		
		-- dock info
		self.dockbar:Draw()
		
		-- radar color
		self.scancolor = {175, 175, 175}

		-- finishing nav
		if self.navto.naving and player.navto.donenaving then
			self.navto.naving = false
			player.navto.donenaving = false
			self.dockbar:Toggle()
			if not player.navto.outoffuel then
				self.systemindex = self.navto.index
				self.systemindexold = self.navto.index
			else
				player.navto.outoffuel = false
			end
		end

		lg.setColor(155, 155, 155, 100)
		lg.rectangle("fill", self.offset, 10, 275, 275) -- backdrop

		for i,v in ipairs(self.lines) do -- radar thing
			lg.setColor(self.scancolor[1], self.scancolor[2], self.scancolor[3], v)

			lg.line(self.offset, 10+i, self.offset+275, 10+i)
			self.lines[i] = lerp2(0, self.lines[i], 0.99)
		end

		-- grid
		lg.setColor(0, 0, 0, 100)
		for i=1,275,3 do --x
			lg.line(self.offset+i, 10, self.offset+i, 10+275)
		end
		for i=1,275,3 do --y
			lg.line(self.offset, 10+i, self.offset+275, 10+i)
		end
		
		-- draw systems
		if self.systemindex < 1 then
			for i,v in ipairs(self.systems) do
				local dx = self.offset + 275*v.x
				local dy = 20 + 275*v.y
				local mx,my = love.mouse.getPosition()

				self.systems[i].a = lerp2(25, v.a, 0.999)
				lg.setColor(255, 255, 255, v.a)
				lg.circle("fill", dx, dy, 5*v.s)

				if dist(dx,dy, mx,my) <= 5*v.s then -- hover
					self.systems[i].a = lerp2(255, v.a, 0.8)
					lg.setColor(255, 255, 255, v.a)
					v.s = cerp(2, v.s, 0.8)

					local text = v.n .. " " .. math.floor((v.x + v.y) * 100)
					
					lg.setColor(0, 0, 0)
					lg.print(text, self.offset + 6, 16)
					lg.setColor(255, 255, 255)
					lg.print(text, self.offset + 5, 15)

					if self.clicked.d and not self.navto.naving and not self.warning then
						player:NavTo(dx-self.offset, dy)
						self.navto.naving = true
						self.navto.x = dx-self.offset
						self.navto.y = dy-10
						self.navto.index = i
						self.dockbar:SetText(v.n .. " " .. math.floor((v.x + v.y) * 100))
						if self.dockbar.enabled then
							self.dockbar:Toggle()
						end
						if cargo:HasContraband() and not self.warning and self.systemindexold ~= self.navto.index then
							self:DisplayWarning()
						end
					end
				else -- unhover
					v.s = cerp(1, v.s, 0.8)
				end

				local ry = 10 + self.lineindex
				if dist(dx,dy, dx,ry) <= 5*v.s then
					self.systems[i].a = lerp2(255, v.a, 0.97)
				end
				if dist(player.x,player.y, player.x,ry) <= 5 then
					player:Blip()
				end

			end -- for
		end
		
		-- draw galaxy
		if self.systemindex > 0 then
			for i,planet in ipairs(self.systems[self.systemindex].galaxy) do
				planet.a = lerp2(25, planet.a, 0.999)
				
				local dx = self.offset + planet.x
				local dy = 10 + planet.y
				local mx,my = love.mouse.getPosition()
				
				-- draw
				if planet.type == "sun" then
					lg.setColor(255, 255, 0, planet.a)
				elseif planet.type == "earth" then
					lg.setColor(0, 255, 255, planet.a)
				else
					lg.setColor(255, 255, 255, planet.a)
				end
				lg.circle("fill", dx, dy, planet.s*planet.scale)
				
				-- moon
				if planet.moon ~= nil then
					lg.setColor(0, 255, 0)
					lg.circle("fill", self.offset+planet.moon.x, 10+planet.moon.y, planet.moon.s)
				end
				
				-- hover
				if dist(dx,dy, mx,my) <= planet.s*planet.scale then
					planet.a = lerp2(255, planet.a, 0.8)
					planet.scale = cerp(2, planet.scale, 0.8)
					if self.clicked.d then
						if player.docked then
							player.docked = false
							player.vel = 0
							player.justdocked = true
						end
						player.navto.planetary = true
						self.navto.planetary = true
						self.navto.pindex = i
					end
				else
					planet.scale = cerp(1, planet.scale, 0.8)
				end
				
				-- blink
				local ry = 10 + self.lineindex
				if dist(dx,dy, dx,ry) <= planet.s then
					planet.a = lerp2(255, planet.a, 0.97)
				end
				if dist(player.x,player.y, player.x,ry) <= 5 then
					player:Blip()
				end
				
				-- debug
				--lg.print(dx .. " : " .. dy, 15, 300+i*15)
			end -- for
		end

		if self.navto.naving then
			lg.setColor(0, 0, 0)
			lg.print("navigating " .. self.navto.x .. "|" .. self.navto.y .. " ...", self.offset+6, 26)
			lg.setColor(255, 255, 255)
			lg.print("navigating " .. self.navto.x .. "|" .. self.navto.y .. " ...", self.offset+5, 25)
		end
		
		-- outline
		lg.setColor(0, 0, 0, 255)
		lg.setLineWidth(1)
		lg.setLineStyle("rough")
		lg.rectangle("line", self.offset, 10, 275, 275)
		lg.setLineWidth(1)
		
		-- cross
		lg.setColor(0, 255, 0, 100)
		lg.line(self.offset+player.x, 10, self.offset+player.x, 285)
		lg.line(self.offset, player.y, self.offset+275, player.y)
		
		-- warning
		local mx,my = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		lg.rectangle("fill", lg.getWidth()/2-100, self.woffset, 200, 75)
		lg.setColor(0, 0, 0)
		lg.rectangle("line", lg.getWidth()/2-100, self.woffset, 200, 75)
		lg.setColor(255, 255, 255)
		lg.print("YOU ARE HAULING CONTRABAND\n  PROCEED?", lg.getWidth()/2-90, self.woffset+15)
		
		-- yes
		lg.setColor(0, 0, 0, 100)
		if mx > lg.getWidth()/2+45 and mx < (lg.getWidth()/2+45)+32 and my > self.woffset+50 and my < self.woffset+50+15 then
			lg.setColor(0, 0, 0, 50)
			if self.clicked.d then
				self.warning = false
				self.navto.naving = true
				player.navto.naving = true
				player.navto.rotating = true
			end
		end
		lg.rectangle("fill", lg.getWidth()/2+45, self.woffset+50, 32, 15)
		lg.setColor(255, 255, 255)
		lg.print("YES", lg.getWidth()/2+50, self.woffset+52)
		-- no
		lg.setColor(0, 0, 0, 100)
		if mx > lg.getWidth()/2-77 and mx < (lg.getWidth()/2-77)+32 and my > self.woffset+50 and my < self.woffset+50+15 then
			lg.setColor(0, 0, 0, 50)
			if self.clicked.d then
				self.warning = false
			end
		end
		lg.rectangle("fill", lg.getWidth()/2-77, self.woffset+50, 32, 15)
		lg.setColor(255, 255, 255)
		lg.print("NO", lg.getWidth()/2-72, self.woffset+52)
		
		self.clicked.d = false
		
	end;

	GetSystemName = function(self)
		return "System"
		--return self.names[math.random(1, #self.names)]
	end;

	Generate = function(self, player)
		math.randomseed(socket.gettime()*1000) -- don't know why this needs to be here. should be global
		-- prevent nil error
		if player then
			player.docked = false
			self.navto.planetary = false
			player.navto.planetary = false
		end
		
		self.systems = {}
		-- generate universe
		for i=1,10 do
			local x = math.random(10, 90)
			local y = math.random(10, 90)
			
			local system = {
				x = x/100;
				y = y/100;
				s = 1;
				a = 155;
				n = self:GetSystemName();
				h = false;
				
				galaxy = {};
			}
			local sun = {x = 275/2, y=275/2, s=9, scale=1, speed=1, type="sun", a=155}
			table.insert(system.galaxy, sun) -- sun
			
			for i=1,math.random(1,4) do ::redo::
				local planet = {}
				planet.x = math.random(30, 245)
				planet.y = math.random(30, 245)
				planet.s = math.random(3, 5) + math.random()
				planet.speed = dist(planet.x,planet.y, sun.x, sun.y)
				planet.rot = math.deg(math.random(360))
				planet.type = "earth"
				planet.scale = 1
				planet.a = 155
				
				if math.random() > 0.5 and not system.h then
					planet.moon = {
						i = math.random(0.5, 1.5);
						dx = math.random(0, 5) + ((planet.s*2)+10);
						dy = math.random(0, 5) + ((planet.s*2)+10);
						x = 0;
						y = 0;
						s = math.random(1, 1.5)
					}
					local _i = math.random(0, 1)
					if _i == 0 then planet.moon.i = math.random(-0.5, -1.5) end
					system.h = true
				end
				
				if dist(planet.x, planet.y, 275/2, 275/2) < 10 or
						dist(planet.x,planet.y, 275/2,275/2) > 100 then
					goto redo
				else
					table.insert(system.galaxy, planet)
				end
			end

			table.insert(self.systems, system)
		end
	end;
	
	Back = function(self, player)
		player.x = self.offset + 275*self.systems[self.systemindex].x - 10
		player.y = 20 + 275*self.systems[self.systemindex].y
		player.docked = false
		
		self.systemindex = 0
	end;
	
	Click = function(self, x, y)
		self.clicked.d = true
		self.clicked.x = x
		self.clicked.y = y
	end;
	
	DisplayWarning = function(self)
		self.warning = true
		self.navto.naving = false
		player.navto.naving = false
		player.navto.rotating = false
	end;
}
