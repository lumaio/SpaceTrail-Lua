extern vec2 image_size;
extern number intensity = 2.0;

vec4 effect(vec4 color, Image tex, vec2 tc, vec2 pc) {
	vec2 offset = vec2(1.0)/image_size;
	color = Texel(tex, tc);

	color.rgb += Texel(tex, tc + intensity*vec2(-offset.x, offset.y)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(0.0, offset.y)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(offset.x, offset.y)).rgb;

	color.rgb += Texel(tex, tc + intensity*vec2(-offset.x, 0.0)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(0.0, 0.0)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(offset.x, 0.0)).rgb;

	color.rgb += Texel(tex, tc + intensity*vec2(-offset.x, -offset.y)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(0.0, -offset.y)).rgb;
	color.rgb += Texel(tex, tc + intensity*vec2(offset.x, -offset.y)).rgb;

	return vec4(color.rgb/9, color.a);
}
