// How much we distort on the x and y axis.
//   from 0 to 1
extern float x_distortion;
extern float y_distortion;
vec2 distort_coords(vec2 point)
{
	// convert to coords we use for barrel distort function
	//   turn 0 -> 1 into -1 -> 1
	point.x = ((point.x * 2.0) - 1.0);
	point.y = ((point.y * -2.0) + 1.0);
	// distort
	point.x = point.x + (point.y * point.y) * point.x * x_distortion;
	point.y = point.y + (point.x * point.x) * point.y * y_distortion;
	// convert back to coords glsl uses
	//   turn -1 -> 1 into 0 -> 1
	point.x = ((point.x + 1.0) / 2.0);
	point.y = ((point.y - 1.0) / -2.0);
	return point;
}
vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _)
{
	vec2 working_coords = distort_coords(tc);
	return Texel(texture, working_coords);
}
