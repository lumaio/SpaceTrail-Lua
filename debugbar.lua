require ("lib.classy")

class "Debugbar"
{
	offset = 10;
	enabled = true;
	
	
	Debugbar = function(self)
		
	end;
	
	Update = function(self, dt)
		if self.enabled then
			self.offset = cerp(-210, self.offset, 0.8)
		else
			self.offset = cerp(10, self.offset, 0.8)
		end
	end;
	
	Draw = function(self)
		lg.setColor(0, 0, 0)
		lg.rectangle("line", 10, lg.getHeight()+self.offset, 175, 200)
		lg.setColor(0, 0, 0, 100)
		lg.rectangle("fill", 10, lg.getHeight()+self.offset, 175, 200)
		
		lg.setColor(255, 255, 255)
		lg.print("DOCKED: " .. tostring(player.docked), 15, lg.getHeight()+self.offset+10)
		lg.print("FPS: " .. love.timer.getFPS(), 15, lg.getHeight()+self.offset+25)
		lg.print("SIZE: {" .. lg.getWidth() .. " : " .. lg.getHeight() .. "}", 15, lg.getHeight()+self.offset+40)
	end;
	
}

class "Debugbutton"
{
	
	position = {x=15, y=-25};
	
	Debugbutton = function(self)
		
	end;
	
	Update = function(self, dt)
		if debug.enabled then
			self.position.y = cerp(-237, self.position.y, 0.8)
		else
			self.position.y = cerp(-25, self.position.y, 0.8)
		end
	end;
	
	Draw = function(self)
		local mpos = {}
		mpos.x, mpos.y = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		if mpos.x > self.position.x and mpos.x < self.position.x + 90 and
				mpos.y > lg.getHeight()+self.position.y and mpos.y < lg.getHeight()+self.position.y + 25 then
			lg.setColor(0, 0, 0, 50)
		end
		
		lg.rectangle("fill", self.position.x, lg.getHeight()+self.position.y, 90, 25)
		lg.setColor(255, 255, 255, 255)
		lg.print("DEBUG", self.position.x+5, lg.getHeight()+self.position.y+5)
		
	end;
	
	Click = function(self, x, y)
		if x > self.position.x and x < self.position.x + 90 and
				y > lg.getHeight()+self.position.y and y < lg.getHeight()+self.position.y + 25 then
			return true
		end
		return false
	end;
	
}
