require ("lib.classy")
require ("navigation")
require ("openradar")
require ("backbutton")
require ("player")
require ("playerdock")
require ("cargodock")
require ("spaceport")
require ("debugbar")

socket = require ("socket")

lg = love.graphics
local sintime = 0

graphics_settings = {
	grid_glow = true;
	grid = true;
	transparency = true;
	stars = true;
}

dust = {}

function love.load()
	
	local modes = love.window.getFullscreenModes(1)
	table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
	if #modes > 0 then
		display = modes[#modes]
		love.window.setMode(display.width-display.width/6, display.height-display.height/6, {resizable=true})
	end
	
	nav = new "Navigation"
	player = new "Player"
	opr = new "OpenRadarButton"
	back = new "BackButton"
	pdock = new "PlayerDock"
	pdb = new "PlayerDockButton"
	cargo = new "CargoDock"
	cdb = new "CargoDockButton"
	port = new "Spaceport"
	debug = new "Debugbar"
	db = new "Debugbutton"

	default_font = lg.newFont("data/ProggyClean.ttf", 16)
	default_font:setFilter("nearest", "nearest")
	lg.setFont(default_font)
	math.randomseed(os.time())
	print(os.time())
	print(math.random())
	
	-- add some cargo for debug purposes
	for i=0,3 do
		cargo:AddCargo(new "Cargo")
	end
	cargo:AddCargo(new "Fuck")
	
	for i=1,300 do
		local _ = {}
		_.x = math.random(50, lg.getWidth()-50)
		_.y = math.random(50, lg.getHeight()-300)
		_.z = math.random(1, 6) + math.random()
		table.insert(dust, _)
	end
end

function love.update(dt)
	sintime = sintime + dt
	
	nav:Update(dt)
	player:Update(dt)
	opr:Update(dt)
	back:Update(dt)
	pdock:Update(dt)
	pdb:Update(dt)
	cargo:Update(dt)
	cdb:Update(dt)
	port:Update(dt)
	debug:Update(dt)
	db:Update(dt)
end

function love.draw()
	
	lg.clear(1, 1, 1)
	
	lg.setColor(130, 200, 255, 3) -- outline blur
	local offx = math.sin(0.3*sintime)*30
	local offz = math.cos(0.3*sintime)*20
	local offy = math.sin(0.1*sintime)*60
	local endscale = 20
	
	for i = 7, 2, -1 do
		if i == 2 then
			i = 1
			love.graphics.setColor(130, 200, 255, 30) -- inner
		end
		
		love.graphics.setLineWidth(i)
		lg.setLineStyle("smooth")
		-- draw lined shape here
		for i=1,lg.getWidth()/endscale do
			lg.line(i*endscale+offx, lg.getHeight()/1.5 + offy, (i*(200+offz))+offx, lg.getHeight()) -- right
			lg.line((lg.getWidth()-i*endscale)+offx, lg.getHeight()/1.5+offy, (lg.getWidth()-i*(200+offz))+offx, lg.getHeight()) --left
		end
	end
	lg.line(0, lg.getHeight()/1.5 + offy, lg.getWidth(), lg.getHeight()/1.5 + offy)
	
	
	
	lg.setColor(144, 0, 32, 255)
	lg.rectangle("fill", 0, 0, lg.getWidth(), lg.getHeight()/1.5+offy)
	lg.setColor(36, 18, 20, 100)
	lg.rectangle("fill", 0, 0, lg.getWidth(), lg.getHeight())
	
	if graphics_settings.stars then
		for i,v in ipairs(dust) do
			lg.setColor(255, 255, 255, 10+v.z*10)
			lg.print("fuck")
			lg.circle("fill", v.x+offx*v.z/5, v.y-offz*v.z/5+offy/v.z, v.z/3)
		end
	end
	
	nav:Draw()
	player:Draw()
	opr:Draw()
	back:Draw()
	pdock:Draw()
	pdb:Draw()
	cargo:Draw()
	cdb:Draw()
	port:Draw()
	debug:Draw()
	db:Draw()
end

function love.keypressed(key)
	if key == "r" then
		nav:Generate(player)
	elseif key == "tab" then
		nav:Toggle()
	end
end

function love.mousepressed(x, y, b)
	if b == 1 then
		nav:Click(x, y)
		back:Click(x, y, nav, player)
		port:Click(x, y)
		if opr:Click(x, y) then nav:Toggle() end
		
		if pdb:Click(x, y) then pdock:Toggle() end
		if cdb:Click(x, y) then cargo:Toggle() end
		if db:Click(x, y) then debug.enabled = not debug.enabled end
	end
end
