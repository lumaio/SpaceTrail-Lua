require ("lib.classy")

class "PlayerDock"
{
	enabled = false;
	offset = 10;
	
	PlayerDock = function(self)
		
	end;
	
	Update = function(self, dt)
		
		if self.enabled then
			self.offset = cerp(10, self.offset, 0.8)
		else
			self.offset = cerp(-285, self.offset, 0.8)
		end
		
	end;
	
	Draw = function(self)
		-- backdrop
		lg.setColor(0, 0, 0, 255)
		lg.rectangle("line", self.offset, 350, 275, 95)
		lg.setColor(0, 0, 0, 100)
		lg.rectangle("fill", self.offset, 350, 275, 95)
		
		-- fuel
		for i,v in ipairs(player.fuel) do
			lg.setColor(255, 0, 0, 100)
			if i == player.fuelindex then
				lg.setColor(155, 255, 0, 100)
			end
			
			lg.rectangle("fill", self.offset+15+(i-1)*(v.max + 10), 365, v.current, 10)
			lg.setColor(0, 0, 0, 255)
			lg.rectangle("line", self.offset+15+(i-1)*(v.max + 10), 365, v.max, 10)
		end
		lg.setColor(255, 255, 255, 255)
		lg.print("F U E L", self.offset+215, 365)
	end;
	
	Toggle = function(self)
		self.enabled = not self.enabled
	end;
}


------------------
-- Toggle Button

class "PlayerDockButton"
{
	position = {x=0, y=354};
	
	PlayerDockButton = function(self)
		--constructor
	end;
	
	Update = function(self, dt)
		if pdock.enabled then
			self.position.x = cerp(286, self.position.x, 0.8)
		else
			self.position.x = cerp(0, self.position.x, 0.8)
		end
	end;
	
	Draw = function(self)
		local mpos = {}
		mpos.x, mpos.y = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		if mpos.x > self.position.x and mpos.x < self.position.x + 25 and
				mpos.y > self.position.y and mpos.y < self.position.y + 90 then
			lg.setColor(0, 0, 0, 50)
		end
		
		lg.rectangle("fill", self.position.x, self.position.y, 25, 90)
		lg.setColor(255, 255, 255, 255)
		lg.print("P\nL\nA\nY\nE\nR", self.position.x+5, self.position.y+5)
		
	end;
	
	Click = function(self, x, y)
		if x > self.position.x and x < self.position.x + 25 and
				y > self.position.y and y < self.position.y + 90 then
			return true
		end
		return false
	end;
	
}
