require ("lib.classy")

function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end

class "Dockbar"
{
	
	enabled = false;
	text = "";
	y = -50;
	timer = 0;
	
	Dockbar = function(self)
		
	end;
	
	Update = function(self, dt)
		
		if self.enabled then
			self.timer = self.timer + dt
			self.y = cerp(10, self.y, 0.87)
			if self.timer > 10 then
				self:Toggle()
				self.timer = 0
			end
		else
			self.y = cerp(-50, self.y, 0.87)
		end
	end;
	
	Draw = function(self)
		local fnt = lg.getFont()
		local dw = fnt:getWidth(self.text)+10
		local dh = fnt:getHeight(self.text)
		
		lg.setColor(155, 0, 0, 100)
		lg.rectangle("fill", math.floor(lg.getWidth()/2-dw/2), self.y, dw, 25)
		lg.setColor(255, 255, 255 ,255)
		lg.rectangle("line", math.floor(lg.getWidth()/2-dw/2), self.y, dw, 25)
		
		
		lg.print(self.text, math.floor(lg.getWidth()/2-dw/2+5), self.y+5)
		
	end;
	
	SetText = function(self, text)
		self.text = text
	end;
	
	Toggle = function(self)
		self.timer = 0
		self.enabled = not self.enabled
	end;
	
}
