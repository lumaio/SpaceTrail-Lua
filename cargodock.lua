require ("lib.classy")


class "CargoDock"
{
	enabled = false;
	
	cargo = {};
	
	offset = lg.getWidth()+10;
	
	CargoDock = function(self)
		
	end;
	
	Update = function(self, dt)
		
		if self.enabled then
			self.offset = cerp(lg.getWidth()-285, self.offset, 0.8)
		else
			self.offset = cerp(lg.getWidth()+10, self.offset, 0.8)
		end
		
	end;
	
	Draw = function(self)
		lg.setColor(0, 0, 0, 100)
		lg.rectangle("fill", self.offset, 10, 275, 300)
		lg.setColor(0, 0, 0)
		lg.rectangle("line", self.offset, 10, 275, 300)
		
		local _ = 1;
		for i,v in ipairs(self.cargo) do
			local mx,my = love.mouse.getPosition()
			
			lg.setColor(0, 0, 0, 100)
			if mx > self.offset+10 and mx < self.offset+10+200 and my > 15+_*15 and my < (15+_*15)+17 then
				lg.setColor(0, 0, 0, 50)
			end
			lg.rectangle("fill", self.offset+10, 15+_*15, 200, 15)
			
			lg.setColor(255, 255, 255)
			lg.print(v.name, self.offset+10, 15+_*15)
			lg.print(v.basevalue, self.offset+75, 15+_*15)
			if v.contraband then
				lg.setColor(255, 0, 0)
				lg.print("CONTRABAND", self.offset+125, 15+_*15)
			end
			
			_ = _ + 1
		end
		
	end;
	
	Toggle = function(self)
		self.enabled = not self.enabled
	end;
	
	AddCargo = function(self, c)
		table.insert(self.cargo, c)
	end;
	
	HasContraband = function(self)
		local c = false
		for i,v in ipairs(self.cargo) do
			if v.contraband then
				c = true
				break;
			end
		end
		return c
	end;
}


------------------
-- Toggle Button

class "CargoDockButton"
{
	position = {x=lg.getWidth()-25, y=14};
	
	CargoDockButton = function(self)
		--constructor
	end;
	
	Update = function(self, dt, dock)
		if cargo.enabled then
			self.position.x = cerp(lg.getWidth()-311, self.position.x, 0.8)
		else
			self.position.x = cerp(lg.getWidth()-25, self.position.x, 0.8)
		end
	end;
	
	Draw = function(self)
		local mpos = {}
		mpos.x, mpos.y = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		if mpos.x > self.position.x and mpos.x < self.position.x + 25 and
				mpos.y > self.position.y and mpos.y < self.position.y + 90 then
			lg.setColor(0, 0, 0, 50)
		end
		
		lg.rectangle("fill", self.position.x, self.position.y, 25, 90)
		lg.setColor(255, 255, 255, 255)
		lg.print("C\nA\nR\nG\nO", self.position.x+5, self.position.y+5)
		
	end;
	
	Click = function(self, x, y)
		if x > self.position.x and x < self.position.x + 25 and
				y > self.position.y and y < self.position.y + 90 then
			return true
		end
		return false
	end;
	
}


--------------------
-- Cargos

struct "Cargo"
{
	name = "default";
	contraband = false;
	basevalue = 20;
	weight = 30;
}

struct "Fuck" : Cargo
{
	name = "fuck";
	contraband = true;
}
