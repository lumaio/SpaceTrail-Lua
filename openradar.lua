require ("lib.classy")

function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end

class "OpenRadarButton"
{
	position = {x=0, y=14};
	
	OpenRadarButton = function(self)
		--constructor
	end;
	
	Update = function(self, dt)
		if nav.enabled then
			self.position.x = cerp(286, self.position.x, 0.8)
		else
			self.position.x = cerp(0, self.position.x, 0.8)
		end
	end;
	
	Draw = function(self)
		local mpos = {}
		mpos.x, mpos.y = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		if mpos.x > self.position.x and mpos.x < self.position.x + 25 and
				mpos.y > self.position.y and mpos.y < self.position.y + 90 then
			lg.setColor(0, 0, 0, 50)
		end
		
		lg.rectangle("fill", self.position.x, self.position.y, 25, 90)
		lg.setColor(255, 255, 255, 255)
		lg.print("R\nA\nD\nA\nR", self.position.x+5, self.position.y+5)
		
	end;
	
	Click = function(self, x, y)
		if x > self.position.x and x < self.position.x + 25 and
				y > self.position.y and y < self.position.y + 90 then
			return true
		end
		return false
	end;
	
}
