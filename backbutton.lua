require ("lib.classy")

function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end

class "BackButton"
{
	position = {x=0, y=14};
	
	BackButton = function(self)
		--constructor
	end;
	
	Update = function(self, dt)
		self.position.y = 285
		if (nav.systemindex > 0) then
			self.position.x = cerp(nav.offset+10, self.position.x, 0.84)
		else
			self.position.x = cerp(-285, self.position.x, 0.84)
		end
	end;
	
	Draw = function(self)
		local mpos = {}
		mpos.x, mpos.y = love.mouse.getPosition()
		
		lg.setColor(0, 0, 0, 100)
		if mpos.x > self.position.x and mpos.x < self.position.x + 125 and
				mpos.y > self.position.y and mpos.y < self.position.y + 25 then
			lg.setColor(0, 0, 0, 50)
		end
		
		lg.rectangle("fill", self.position.x, self.position.y, 125, 25)
		lg.setColor(255, 255, 255, 255)
		lg.print("BACK TO SYSTEMS", self.position.x+5, self.position.y+5)
		
	end;
	
	Click = function(self, x, y)
		if x > self.position.x and x < self.position.x + 125 and
				y > self.position.y and y < self.position.y + 25 then
			nav.navto.planetary = false
			nav.navto.pindex = false
			player.navto.planetary = false
			port.enabled = false
			nav:Back(player)
		end
	end;
	
}
