require ("lib.classy")
require "dockbar"

function cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end
function lerp2(a,b,t) return a+(b-a)*t end

class "Player"
{
	x = 100;
	y = 100;
	r = 0;
	a = 255;
	vel = 0;
	navto = {};
	offset=0;
	image;
	
	docked = false;
	justdocked = false;
	fuelindex = 5;
	maxfuel = 50;
	fuel = {};
	
	credits = 0;
	

	Player = function(self)
		self.image = lg.newImage("data/player.png")
		self.image:setFilter("nearest", "nearest")
		
		for i=1,5 do
			local _f = {
				current=30;
				max=30;
			}
			table.insert(self.fuel, _f)
		end
		
		self.navto = {
			naving = false;
			donenaving = false;
			outoffuel = false;
			rotating = false;
			rot = 0;
			planetary = false;
			pindex = 0;
			x = 0;
			y = 0;
		}
	end;

	Update = function(self, dt)
		-- concurrency?
		self.offset = offset
		-- blip
		self.a = lerp2(155, self.a, 0.999)
		
		if self.justdocked then
			port:Toggle()
			self.justdocked = false
		end
		
		if not self.navto.planetary then --system
			
			
			if not self.navto.naving and not self.navto.rotating then
				if self.vel > 0 then
					self.vel = self.vel - 10*dt
				else
					self.vel = 0
				end
				return
			end
			
			-- fuel, not needed for galaxy flight
			if self.navto.naving and not self.navto.donenaving and not self.navto.rotating then
				self.fuel[self.fuelindex].current = self.fuel[self.fuelindex].current - dt*5
				if self.fuel[self.fuelindex].current < 0 then
					if self.fuelindex > 1 then
						self.fuelindex = self.fuelindex - 1
					else
						self.navto.naving = false
						self.navto.donenaving = true
						self.navto.outoffuel = true
					end
					self.fuel[self.fuelindex].current = self.fuel[self.fuelindex].current + self.fuel[self.fuelindex+1].current
					self.fuel[self.fuelindex+1].current = 0
				end
			end
			
			
			-- snap to correct rotation to avoid floating point errors
			if self.r > self.navto.rot-0.02 and self.r < self.navto.rot+0.02 then
				self.r = self.navto.rot
			end
			-- rotate and stuff
			if self.navto.rotating then
				if self.r ~= self.navto.rot then
					self.r = self.navto.rot
				else
					self.navto.rotating = false
				end
			end
			-- actually move
			if self.navto.naving and not self.navto.rotating then
				if self.vel < 50 then
					self.vel = self.vel + 30*dt
				end
				
				if self.x > self.navto.x-3 and self.x < self.navto.x+3 and
						self.y > self.navto.y-3 and self.y < self.navto.y+3 then
					self.navto.naving = false
					self.navto.rotating = false
					self.navto.donenaving = true
					self.vel = 0
				end
				--[[
				self.x = self.x - math.cos(self.navto.rot) * self.vel*dt
				self.y = self.y - math.sin(self.navto.rot) * self.vel*dt
				]]
				self.x = lerp2(self.navto.x, self.x, 0.99)
				self.y = lerp2(self.navto.y, self.y, 0.99)
			end
			
		else --planetary/galaxy
			local _planet = nav.systems[nav.systemindex].galaxy[nav.navto.pindex]
			
			if not self.docked then
				local _r = math.atan2(self.y-_planet.y-10, self.x-_planet.x)
				
				if self.vel < _planet.speed+10 then
					self.vel = self.vel + 30*dt
				else
					self.vel = _planet.speed
				end
				
				self.r = lerp2(_r, self.r, 0.99)
				self.x = self.x - math.cos(_r) * self.vel*dt
				self.y = self.y - math.sin(_r) * self.vel*dt
				
				if dist(self.x,self.y, _planet.x,_planet.y+10) <= _planet.s/2 then
					self.justdocked = true
					self.docked = true
				end
				
			else
				self.vel = 0
				self.x = _planet.x
				self.y = _planet.y+10
			end
			
		end

	end;

	Draw = function(self)
		lg.setColor(255, 75, 75, self.a)
	end;

	NavTo = function(self, x, y)
		self.navto.naving = true
		self.navto.rotating = true
		self.navto.x = x
		self.navto.y = y
		self.navto.rot = math.atan2(self.y - (y), self.x - (x))
	end;

	Blip = function(self)
		self.a = 255
	end;
}
