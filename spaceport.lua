require ("lib.classy")

class "Spaceport"
{
	enabled = true;
	bank = false;
	offset = lg.getHeight()+10;
	boffset = lg.getWidth()/2;
	
	gwidth = 175;
	gy1 = 175;
	gy2 = 200;
	
	options = {
		{
			text="Galactic Bank";
			click = function(port)
				port.bank = not port.bank
			end;
		};
		{
			text="Event Options?";
			click = function(port)
				
			end;
		};
		{
			text = "Leave";
			click = function(port)
				port:Toggle()
				nav.navto.planetary = false
				player.navto.planetary = false
				player.docked = false
				print("fuck " .. tostring(player.docked))
			end;
		};
	};
	
	clicked = {
		d = false;
		x = 0;
		y = 0;
	};
	
	Spaceport = function(self)
		
	end;
	
	Update = function(self, dt)
		if self.bank then
			self.gy1 = cerp(45, self.gy1, 0.8)
			self.gy2 = cerp(105, self.gy2, 0.8)
			self.gwidth = cerp(175, self.gwidth, 0.8)
			self.boffset = cerp(lg.getWidth()/2-190, self.boffset, 0.8)
		else
			self.gy1 = cerp(175, self.gy1, 0.75)
			self.gy2 = cerp(200, self.gy2, 0.75)
			self.gwidth = cerp(150, self.gwidth, 0.75)
			self.boffset = cerp(lg.getWidth()/2, self.boffset, 0.8)
		end
		if self.enabled then
			self.offset = cerp(lg.getHeight()-400, self.offset, 0.8)
		else
			self.offset = cerp(lg.getHeight()+10, self.offset, 0.8)
		end
	end;
	
	Draw = function(self, player, nav)
		lg.setColor(0, 0, 0)
		lg.rectangle("line", lg.getWidth()/2-600/2, self.offset, 600, 225)
		--lg.setColor(51, 204, 255, 10)
		lg.setColor(0, 0, 0, 100)
		lg.rectangle("fill", lg.getWidth()/2-600/2, self.offset, 600, 225)
		
		for i,v in ipairs(self.options) do
			local _width = default_font:getWidth(v.text)
			local mx,my = love.mouse.getPosition()
			local x,y = self.boffset-200/2, self.offset+10+i*30
			local w,h = 200, 25
			
			lg.setColor(0, 0, 0, 100)
			if mx > x and mx < x+w and my > y and my < y+h then
				if self.bank then
					if i == 1 or i == #self.options then
						lg.setColor(0, 0, 0, 50)
						if self.clicked.d then
							v.click(self, player, nav)
							print("shit")
						end
					end
				else
					lg.setColor(0, 0, 0, 50)
					if self.clicked.d then
						v.click(self, player, nav)
						print("shit")
					end
				end
			end
			lg.rectangle("fill", x, y, w, h)
			lg.setColor(255, 255, 255)
			lg.print(v.text, math.floor(self.boffset-_width/2), math.floor(self.offset+15+i*30))
		end
		
		if self.bank and self.boffset < lg.getWidth()/2-180 then
		end
		
		lg.setColor(0, 0, 0,100)
		lg.rectangle("fill", lg.getWidth()/2-self.gwidth/2, self.offset+self.gy1-5, self.gwidth, 25)
		lg.rectangle("fill", lg.getWidth()/2-self.gwidth/2, self.offset+self.gy2-5, self.gwidth, 25)
		
		lg.setColor(255, 255, 255)
		local cx = (lg.getWidth()/2-self.gwidth/2) + self.gwidth/2
		
		lg.print("Deposite", cx - lg.getFont():getWidth("Deposite")/2, self.offset+self.gy1)
		lg.print("Withdraw", cx - lg.getFont():getWidth("Withdraw")/2, self.offset+self.gy2)
		
		self.clicked.d = false
	end;
	
	Click = function(self)
		self.clicked.d = true
	end;
	
	Toggle = function(self)
		self.bank = false
		self.enabled = not self.enabled
	end;
}
