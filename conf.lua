function love.conf(t)
	t.title = "Space Trail"
	t.window.width = 1280
	t.window.height = 720
	t.window.resizable = true
	t.window.vsync = true
end
